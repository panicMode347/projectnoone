import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class App extends React.Component {
  render() {
    return (
      <LinearGradient
        colors={['#3EB7CD', '#0081A7', '#4d006b']}
        style={styles.container}
        start={{x: 1.5, y: 0}}
        end={{x: 1, y: 1}}>
        <Image source={require('./cookie.png')} style={styles.cookie} />

        <Text style={styles.welcome}>Welcome</Text>

        <TextInput
          style={styles.input}
          placeholder="Username/E-mail"
          keyboardType="email-address"
          placeholderTextColor="#00A3D3"
          returnKeyType="next"
          onSubmitEditing={() => this.passwordInput.focus()}
          autoCapitalize="none"
          autoCorrect={false}
        />

        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="#00A3D3"
          secureTextEntry
          returnKeyType="go"
          ref={input => (this.passwordInput = input)}
        />

        <TouchableOpacity style={styles.forgotContainer}>
          <Text style={styles.forgotText}>Forgot password?</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonContainer}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    fontSize: width * 0.1,
    fontWeight: 'bold',
    color: '#78D9F6',
    marginBottom: height * 0.02,
  },
  cookie: {
    width: width * 0.3,
    height: height * 0.2,
    marginBottom: height * 0.02,
  },
  input: {
    width: width * 0.8,
    height: height * 0.08,
    marginBottom: height * 0.02,
    borderStyle: 'solid',
    borderWidth: width * 0.005,
    borderColor: '#6de4ed',
    borderRadius: width * 0.5,
    fontSize: width * 0.04,
    paddingLeft: width * 0.05,
    color: '#0896bf',
  },
  forgotContainer: {
    width: width * 0.8,
    display: 'flex',
    alignItems: 'flex-end',
    marginBottom: height * 0.02,
    marginRight: width * 0.1,
  },
  forgotText: {
    color: '#6de4ed',
    fontSize: width * 0.03,
  },
  buttonContainer: {
    width: width * 0.8,
    height: height * 0.08,
    backgroundColor: '#6de4ed',
    borderRadius: width * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: width * 0.05,
    color: '#0c617a',
  },
});
